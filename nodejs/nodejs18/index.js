
import axios from 'axios';

let config = {
  method: 'post',
  maxBodyLength: Infinity,
  url: 'https://jsonplaceholder.typicode.com/posts',
  headers: { }
};

exports.getPosts = async () =>{
        return axios.request(config)
}
