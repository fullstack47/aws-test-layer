[ -d dist ] && rmdir -r dist
mkdir -p dist
cd nodejs/nodejs18 
npm i
cd ..
cd ..
zip -r dist/layer.zip nodejs

aws lambda publish-layer-version --layer-name test-layer \
    --description "My layer" \
    --license-info "MIT" \
    --zip-file fileb://dist/layer.zip \
    --compatible-runtimes nodejs18.x 